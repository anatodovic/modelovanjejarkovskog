#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#define FIELD_SIZE 15
#define LINE_SIZE 100

#define SOURCE_FILE "jablunka.list"
#define DEST_FILE "Asteroidi.txt"

#ifdef linux
	#define GNUPLOTPATH "gnuplot"
#else
	#define GNUPLOTPATH "C:/MinGW/bin/gnuplot/bin/gnuplot"
#endif

int numOfBins;
int numOfBinsLeft;
int numOfBinsRight;
int asteroidsPerBin;

struct Asteroid
{
    double X;
    double Y;
};

struct FittingLineParameters
{
    double c0;
    double c1;
    double greska;
    int num_of_bins;
};

typedef enum
{
    SumByX = 0,
    SumByY,
    SumByXX,
    SumByYY,
    SumByXY,
	SumByXc,
	SumByXXx,
	SumByYXx
} SUM_OPTION;


int numOfAsteroids = 7430;

int jablunkaIndex = 0;
struct Asteroid jablunka;


typedef enum
{
    PickCenter = 0,
    NonWeightedCenter,
    WeightedCenter
} CENTERING;

typedef enum
{
    Right = 0,
    Left,
    RightAndLeft,
    CombinedWithCenter
} FITTING;

//fit jablunka.list 290 NonWeightedCenter Left
struct Task
{
    char filename[50];
    int num_of_asteroids;
    CENTERING centering;
    FITTING fitting;
};

int sortByY (const void * a, const void * b)
{
    return ( ((double)(((struct Asteroid*)a))->Y >= (double)(((struct Asteroid*)b)->Y)) ? 1 : -1);
}

int sortByX (const void * a, const void * b)
{
    return ( ((double)(((struct Asteroid*)a))->X >= (double)(((struct Asteroid*)b)->X)) ? 1 : -1);
}

/*
Function: parseFamilyFromFile
Description: This function will parse asteroids from jablunka.txt
-
1. broj (oznaka) asteroida - label
2. apsolutna magnituda, H
3. velika poluosa, a - semi_major_axes
4. ekscentricitet, e - eccentricity
5. sinus nagiba, sin(i) - sin_i
6. prečnik, D - diametar
7. greška poluprečnika, sigma(D) - sigma_D

*/
int parceFamilyFromFile(struct Asteroid *asteroids, char *source_str, int *numOfAsteroids)
{
    FILE *source, *dest;

    char label[FIELD_SIZE],
         absolute_magnitude[FIELD_SIZE],
         semi_major_axes[FIELD_SIZE],
         eccentricity[FIELD_SIZE],
         sin_i[FIELD_SIZE],
         diametar[FIELD_SIZE],
         sigma_D[FIELD_SIZE];
    char line[LINE_SIZE];
    int  i=0;

    source = fopen (source_str, "r");
    if(source==NULL)
    {
        printf("error: file %s could not be opened for reading.\n", source_str);
        return -1;
    }

    dest = fopen(DEST_FILE, "w+");
    if(source==NULL)
    {
        printf("error: file %s could not be opened for writing.\n", DEST_FILE);
        return -1;
    }
    
    while (fgets(line, sizeof(line), source)) 
    {
        double x, y;
        sscanf(line, "%s %s %s %s %s %s %s", label, absolute_magnitude, semi_major_axes, eccentricity, sin_i, diametar, sigma_D);
  
        x = atof(semi_major_axes);
        y = 1/atof(diametar);
        asteroids[i].X=x;
        asteroids[i].Y=y;

        fprintf(dest, "%f, %f\n", x, y);
        i++;
    }

    if(i<(*numOfAsteroids) || (*numOfAsteroids)==0)
    {
        printf("warnning: number of asteroids set based on num of asteroids in file\n          ");
        (*numOfAsteroids) = i;
        printf("number of asteroids: %d", (*numOfAsteroids));
    }

    fclose(source);
    fclose(dest);
    return 0;
}

void printToFile(const char *srcDest, struct Asteroid *asteroids, int numOfAsteroids)
{
    int i;
    FILE *dest = fopen(srcDest, "w+");

    for(i=0; i<numOfAsteroids; i++)
    {
        fprintf(dest,"%f, %f\n", asteroids[i].X, asteroids[i].Y);
    }

    fclose(dest);
}

void printFittingLine(struct FittingLineParameters *fittingLine)
{
    printf("-------------------------------\n");
    printf("best fit:  y=%f%s%f * x\n",fittingLine->c0, (fittingLine->c1>0 ? "+" : ""), fittingLine->c1);
    printf("error: %f\n", fittingLine->greska);
    //printf("broj binova: %d\n",fittingLine->num_of_bins);
    printf("-------------------------------\n");
}

/*
Function: sum
Description: This function will sum elements of array X and/or Y from index endIndex to index endIndex
*/
double sum(int endIndex, struct Asteroid *asteroids, SUM_OPTION option, double x)
{
    int i;
    double sum = 0.0;

    for(i=0; i<endIndex; i++)
    {
        switch (option)
        {
            case SumByXY:
                    sum = asteroids[i].X*asteroids[i].Y + sum;
                    break;
            case SumByX:
                    sum = asteroids[i].X + sum;
                    break;
            case SumByY:
                    sum = asteroids[i].Y + sum;
                    break;
            case SumByXX:
                    sum = asteroids[i].X*asteroids[i].X + sum;
                    break;
            case SumByYY:
                    sum = asteroids[i].Y*asteroids[i].Y + sum;
                    break;
            case SumByYXx:
                    sum = asteroids[i].Y*(asteroids[i].X-x) + sum;
                    break;
            case SumByXc:
                    sum = (asteroids[i].X-x) + sum;
                    break;
            case SumByXXx:
                    sum = (asteroids[i].X-x) * (asteroids[i].X-x) + sum;
                    break;
            default:
                    printf("ERROR!!!");
                    break;
            }
    }

    return sum;
}

/*
Function: toFit(int startIndex, int endIndex)
Description: This function will calculate c0 and c1 to determine line that fits array of dots
*/
void toFit(int endIndex, struct Asteroid *asteroids, struct FittingLineParameters *parameters)
{
    double c0,c1;

    // to find y=c0+c1*x, we have to solve this system:
    //c0*m1+c1*n1=k1 => c0*m1 = k1-c1*n1
    //c0*m2+c1*n2=k2 => k1*m2/m1 - c1*n1*m2/m1 +c1*n2 = k2 => (k2-(k1*m2)/m1)/(n2-(n1*m2)/m1)
    double m1, n1, k1;
    double m2, n2, k2;

    m1 = (double)endIndex;
    n1= sum(endIndex, asteroids, SumByX, 0.0);
    k1 = sum(endIndex, asteroids, SumByY, 0.0);

    m2 = sum(endIndex, asteroids, SumByX, 0.0);
    n2= sum(endIndex, asteroids, SumByXX, 0.0);
    k2 = sum(endIndex, asteroids, SumByXY, 0.0);

	printf("dddd %f %f %f %f %f %f\n", m1, n1, k1, m2, n2, k2);
	printf("");
    //c1 = (k2-(k1*m2)/m1)/(n2-(n1*m2)/m1);
    c1 = (k2*m1-(k1*m2))/(n2*m1-(n1*m2));
    //printf("c1 = %f\n", c1);
    c0 = (sum(endIndex, asteroids, SumByY, 0.0) - c1*sum(endIndex, asteroids, SumByX, 0.0))/(double)endIndex;
    //printf("c0 = %f\n", c0);
    //printf("prava: y=%f+%fx\n", c0, c1); 

    parameters->c0 = c0;
    parameters->c1 = c1;
}

void testToFit()
{
    struct FittingLineParameters parameters;
	struct Asteroid testAsteroids[3] = {{0.098658,2.259898},{
0.174338, 2.267947},{
	0.232558, 2.255470}};
    //struct Asteroid testAsteroids[3] = {{2.2,0.2},{2.3,0.3},{2.3,0.4}};
	/*{{2.259898, 0.098658},{
2.267947, 0.174338},{
2.255470, 0.232558}/*,{
2.260988, 0.257732},{
2.258927, 0.272628},{
2.258851, 0.298864},{
2.269258, 0.316156},{
2.270491, 0.344828},{
2.254520, 0.361925},{
2.242544, 0.391083},{
2.269492, 0.432713},{
2.256020, 0.496771},{
2.310259, 0.098658}};*/

    toFit(3,testAsteroids, &parameters);

    printf("testToFit: y=%f+%fx\n", parameters.c0, parameters.c1); 
}

void toFitTogether(struct Asteroid *selectedAsteroidsLeft, int num_of_bins_left, struct Asteroid *selectedAsteroidsRight, int num_of_bins_right, double x, double *k, double *y)
{
	double A,B,C,D,E,F;
	//(SumYXx + SumYXx) + k (SumXXx-SumXXx) - y (SumXx - SumXx) = 0
	//(SumY + SumY)     + k (SumXx-SumXx)   - y (n+m) = 0
	//A+kB=yC -> A/C+k(B/C)=y  A/C-D/F = k (E/F-B/C)
	//D+kE=yF -> D/F+k(E/F)=y
	//k=(A/C-D/F)/(E/F-B/C)
	//y=D/F+k(E/F)
	A = sum(num_of_bins_left, selectedAsteroidsLeft, SumByYXx, x) + sum(num_of_bins_right, selectedAsteroidsRight, SumByYXx, x);
	B = sum(num_of_bins_left, selectedAsteroidsLeft, SumByXXx, x) - sum(num_of_bins_right, selectedAsteroidsRight, SumByXXx, x);
	C = sum(num_of_bins_left, selectedAsteroidsLeft, SumByXc, x) - sum(num_of_bins_right, selectedAsteroidsRight, SumByXc, x);
	D = sum(num_of_bins_left, selectedAsteroidsLeft, SumByY, x) + sum(num_of_bins_right, selectedAsteroidsRight, SumByY, x);
	E = C;
	F = num_of_bins_left + num_of_bins_right;
	
    (*k)=(A/C-D/F)/(E/F-B/C);
	(*y)=(D/F)+(*k)*(E/F);
}

int findNonWeightedCenter(struct Asteroid *asteroids, double *centar, int numOfAsteroids)
{
    int i;
    double sum = 0.0;

    for(i=0; i<numOfAsteroids; i++)
    {
        sum = sum + asteroids[i].X;
    }

    *centar = sum/numOfAsteroids;

    for(i=0; i<numOfAsteroids; i++)
    {
        if(asteroids[i].X < *centar && asteroids[i+1].X > *centar)
        {       
            //printf("NonWeightedCenter = %f [Index = %d]\n", *centar, i);
            return i;
        }
    }

    return -1;
}

int findWeightedCenter(struct Asteroid *asteroids, double *centar, int numOfAsteroids)
{
    int i;
    double sum = 0.0;
    double sumD = 0.0;

    for(i=0; i<numOfAsteroids; i++)
    {
        sumD = sumD + pow(1/asteroids[i].Y, 3);
    }

    for(i=0; i<numOfAsteroids; i++)
    {
        sum = sum + asteroids[i].X/pow(asteroids[i].Y, 3);
    }

    *centar = sum/sumD;
    
    for(i=0; i<numOfAsteroids; i++)
    {
        if(asteroids[i].X < *centar && asteroids[i+1].X > *centar)
        {        
            //printf("WeightedCenter = %f [Index = %d]\n", *centar, i);
            return i;
        }
    }

    return -1;
}

int findNextToPickedCenter(struct Asteroid *asteroids, double center, int numOfAsteroids)
{
    int i;
    // to find Jablunka, we will find asteroid with smalest value of y (this will be the biggest asteroid 1/D !!!
    //qsort by Y axis to devide in bins
    //qsort(asteroids, numOfAsteroids, sizeof(struct Asteroid), sortByY);

    //qsort by X axis 
    qsort(asteroids, numOfAsteroids, sizeof(struct Asteroid), sortByX);

    //find Jablunka's index
    for(i=0; i<numOfAsteroids; i++)
    {
        if(center > asteroids[i].X && center < asteroids[i+1].X)
        {
            printf("Centar=%f\nCentarIndex=%d\n", center, i);
            return i;
        }
    }

    printf("Error: Center not found. Chech if center you entered is within range of asteroids\nx axis: [%f, %f]\n", asteroids[0].X, asteroids[numOfAsteroids-1].X);

    qsort(asteroids, numOfAsteroids, sizeof(struct Asteroid), sortByY);

    return -1;
}

struct Asteroid findMinInArray(struct Asteroid *asteroids, int size)
{
    int i;
    struct Asteroid minAsteroid;

    minAsteroid.X = asteroids[0].X;
    minAsteroid.Y = asteroids[0].Y;

    for(i=0; i<size; i++)
    {
        if(minAsteroid.X > asteroids[i].X)
            minAsteroid.X = asteroids[i].X;
    }
    //printf("minAsteroid = %f, %f\n", minAsteroid.X, minAsteroid.Y);
    return minAsteroid;
}
int findMinIndex(struct Asteroid *asteroids, int size)
{
    int i;
    int minAsteroid=0;

    for(i=0; i<size; i++)
    {
        if(asteroids[minAsteroid].X > asteroids[i].X)
            minAsteroid=i;
    }
    return minAsteroid;
}

struct Asteroid findMaxInArray(struct Asteroid *asteroids, int size)
{
    int i;
    struct Asteroid maxAsteroid;

    maxAsteroid.X = asteroids[0].X;
    maxAsteroid.Y = asteroids[0].Y;

    for(i=0; i<size; i++)
    {
        if(maxAsteroid.X < asteroids[i].X)
            maxAsteroid.X = asteroids[i].X;
    }
    printf("maxAsteroid = %f, %f\n", maxAsteroid.X, maxAsteroid.Y);
    return maxAsteroid;
}

int findMaxIndex(struct Asteroid *asteroids, int size)
{
    int i;
    int maxAsteroid=0;
	//int maxSec=0;

    for(i=0; i<size; i++)
    {
        if(asteroids[maxAsteroid].X < asteroids[i].X)
		{
			//maxSec = maxAsteroid;
            maxAsteroid=i;
		}
    }
	
    //return maxSec;
	return maxAsteroid;
}

void takeNextChunk(struct Asteroid *asteroids, int start, int size, struct Asteroid *chunk)
{
    int i;

    for(i=0; i<size; i++)
    {
        chunk[i].X = asteroids[start+i].X;
        chunk[i].Y = asteroids[start+i].Y;
        //printf("chunk[%d] = %f, %f\n", i, chunk[i].X, chunk[i].Y);
    }

}

void deterimineLeftBins(struct Asteroid *asteroids, int end, int numOfAsteroids, int num_of_bins, struct Asteroid *selectedAsteroids)
{
    int i = 0, j = 0;
    int numberOfAsteroidsInBin;
    struct Asteroid peakAsteroid;

    numberOfAsteroidsInBin = (int)(end/num_of_bins);

    qsort(asteroids, end, sizeof(struct Asteroid), sortByY);

    peakAsteroid.X = asteroids[0].X;
    peakAsteroid.Y = asteroids[0].Y;
	//printf("peak %f %f\n", peakAsteroid.X, peakAsteroid.Y);

    while(i<=num_of_bins)
    {
        struct Asteroid *chunkOfAsteroids = (struct Asteroid*)malloc(((int)(end/num_of_bins)+2)*sizeof(struct Asteroid));
        takeNextChunk(asteroids, i*numberOfAsteroidsInBin, numberOfAsteroidsInBin, chunkOfAsteroids);

        //selectedAsteroids[i].X = findMinInArray(chunkOfAsteroids, numberOfAsteroidsInBin).X;
        //selectedAsteroids[i].Y = findMinInArray(chunkOfAsteroids, numberOfAsteroidsInBin).Y;
		int minIndex = findMinIndex(chunkOfAsteroids, numberOfAsteroidsInBin);
		selectedAsteroids[i].X = chunkOfAsteroids[minIndex].X;
		selectedAsteroids[i].Y = chunkOfAsteroids[minIndex].Y;
		
        //printf("%f, %f\n", selectedAsteroids[i].X,selectedAsteroids[i].Y);
        i++;
        free(chunkOfAsteroids);
    }
	
    selectedAsteroids[num_of_bins].X=peakAsteroid.X;
    selectedAsteroids[num_of_bins].Y=peakAsteroid.Y;
}


void deterimineRightBins(struct Asteroid *asteroids, int start, int numOfAsteroids, int num_of_bins, struct Asteroid *selectedAsteroids)
{
    int i = 0;
    int numberOfAsteroidsInBin;
    struct Asteroid peakAsteroid;

    numberOfAsteroidsInBin = (int)(abs(numOfAsteroids-start)/num_of_bins);

    qsort(asteroids, numOfAsteroids-start, sizeof(struct Asteroid), sortByY);

    peakAsteroid.X = asteroids[0].X;
    peakAsteroid.Y = asteroids[0].Y;

    while(i<num_of_bins)
    {
        struct Asteroid *chunkOfAsteroids = (struct Asteroid*)malloc((numberOfAsteroidsInBin+1)*sizeof(struct Asteroid));
        takeNextChunk(asteroids, i*numberOfAsteroidsInBin, numberOfAsteroidsInBin, chunkOfAsteroids);

        //selectedAsteroids[i].X = findMaxInArray(chunkOfAsteroids, numberOfAsteroidsInBin).X;
        //selectedAsteroids[i].Y = findMaxInArray(chunkOfAsteroids, numberOfAsteroidsInBin).Y;
		int maxIndex = findMaxIndex(chunkOfAsteroids, numberOfAsteroidsInBin);
		selectedAsteroids[i].X = chunkOfAsteroids[maxIndex].X;
		selectedAsteroids[i].Y = chunkOfAsteroids[maxIndex].Y;

        printf("right asteroid: %f, %f\n", selectedAsteroids[i].X,selectedAsteroids[i].Y);
		free(chunkOfAsteroids);
        i++;
    }

    selectedAsteroids[num_of_bins].X = peakAsteroid.X;
    selectedAsteroids[num_of_bins].Y = peakAsteroid.Y;
}

double sumOfDeviations(struct Asteroid* selectedAsteroids, double c0, double c1, int n)
{
    int i;
    double F = 0.0;
    
    for(i=0;i<n;i++)
        F = F + (selectedAsteroids->Y-c0-c1*selectedAsteroids->X)*(selectedAsteroids->Y-c0-c1*selectedAsteroids->X);
	printf("GRESKA %f\n", F);

    return F;
}

double sumOfDeviations2(struct Asteroid* selectedAsteroids, double c0, double c1, int n)
{
    int i;
    double F = 0.0;
    
	//printf("ggg %f %f\n", c0, c1);
    for(i=0;i<n;i++)
	{
		//printf("ggg %f %f\n", selectedAsteroids[i].Y, selectedAsteroids[i].X);
		//printf("ggg %f", F);
        F = F + abs(selectedAsteroids[i].Y-c0*selectedAsteroids[i].X-c1)/sqrt(1+c0*c0);
	}
	printf("GRESKA2 %f\n", F);

    return F;
}


int leftFit(struct Asteroid *asteroids, int numOfAsteroids,
                int centarIndex, struct FittingLineParameters *fittingLineLeft, struct Asteroid *selectedAsteroids)
{
	int i;
    struct FittingLineParameters fittingLineLeft2;
    fittingLineLeft->greska = 1.0;
    	
	struct Asteroid *selectedAsteroids2 = (struct Asteroid*)malloc((int)(numOfBinsLeft+1)*sizeof(struct Asteroid));	
		
    deterimineLeftBins(asteroids, centarIndex, numOfAsteroids, numOfBinsLeft, selectedAsteroids);

	for(i=0;i<numOfBinsLeft+1;i++)
	{
		selectedAsteroids2[i].X=selectedAsteroids[i].Y;
		selectedAsteroids2[i].Y=selectedAsteroids[i].X;
	}
	
	toFit(numOfBinsLeft+1, selectedAsteroids2, &fittingLineLeft2);

	fittingLineLeft->c0 = -fittingLineLeft2.c0/fittingLineLeft2.c1;
    fittingLineLeft->c1 = 1/fittingLineLeft2.c1;
		
	fittingLineLeft->greska = sqrt(sumOfDeviations(selectedAsteroids, fittingLineLeft->c0, fittingLineLeft->c1, numOfBinsLeft+1)/(numOfBinsLeft+1));
	
	printToFile("IzabraniLevi.txt", selectedAsteroids, numOfBinsLeft+1);
    
	printFittingLine(fittingLineLeft);

    return 0;
}

int rightFit(struct Asteroid *asteroids, int numOfAsteroids, int centarIndex,  struct FittingLineParameters *fittingLineRight, struct Asteroid *selectedAsteroids)
{
	int i;
    struct FittingLineParameters fittingLineRight2;
    fittingLineRight->greska = 1.0;
    	
	struct Asteroid *selectedAsteroids2 = (struct Asteroid*)malloc((int)(numOfBinsRight+1)*sizeof(struct Asteroid));	
		
    deterimineRightBins(asteroids, centarIndex, numOfAsteroids, numOfBinsRight, selectedAsteroids);
	
	
	for(i=0;i<numOfBinsRight+1;i++)
	{
		selectedAsteroids2[i].X=selectedAsteroids[i].Y;
		selectedAsteroids2[i].Y=selectedAsteroids[i].X;

	}
	
	toFit(numOfBinsRight+1, selectedAsteroids2, &fittingLineRight2);
		
	fittingLineRight->c0 = -fittingLineRight2.c0/fittingLineRight2.c1;
    fittingLineRight->c1 = 1/fittingLineRight2.c1;
	
	fittingLineRight->greska = sqrt(sumOfDeviations(selectedAsteroids, fittingLineRight->c0, fittingLineRight->c1, numOfBinsRight+1)/(numOfBinsRight+1));
    printf("fitting line left: y= %f + %f * x\ngreska %f\n", fittingLineRight2.c0, fittingLineRight2.c1, fittingLineRight2.greska);

	printToFile("IzabraniDesni.txt", selectedAsteroids, numOfBinsRight+1);
	free(selectedAsteroids);
    
    printFittingLine(fittingLineRight);

    return 0;
}

double calculateErrorOnXforY1km(struct FittingLineParameters fittingLine)
{
     //greska:
     //fittingLineFinal.c0 + fittingLineFinal.c1 * x +- fittingLineFinal.greska = 1
     // this should calculate deviation from value of x for y=1km
     double x1 = (1-fittingLine.c0+fittingLine.greska)/fittingLine.c1;
     double x2 = (1-fittingLine.c0-fittingLine.greska)/fittingLine.c1;
	 double error = fabs(fabs(x1)-fabs(x2))/2;

     return error;
    
}   

void calculateAge(struct FittingLineParameters fittingLineFinal, double center, int *age, int *ageError)
{
    printFittingLine(&fittingLineFinal);

     double errorRight = calculateErrorOnXforY1km(fittingLineFinal);

     // vrednost a (tj.x) za 1/D tj. y=1/D; koristimo levu pravu
     // ako znamo da Jarkovski promeni veliku poluosu asteroida precnika 1km za 0.0005AJ, onda: 
     // promena velike poluose   ----  x godina
     // 0.0005                   ----  1 milion godina
    
     double a_endDesno = center;
     double a_endLevo = 1/fittingLineFinal.c1 - fittingLineFinal.c0/fittingLineFinal.c1;
     
     *age = (int)(fabs(a_endLevo-a_endDesno)/0.00053);
     *ageError = (int)(errorRight/0.00053);

     printf("Age of a family: %d +/- %d millions of years\n", *age, *ageError);
     printf("-------------------------------\n");

} 


void fittingRightAndLeft(struct Asteroid *asteroids, int numOfAsteroids, int centarIndex)
{
    int maxNumOfBinsLeft;
    int maxNumOfBinsRight;
	struct Asteroid *selectedAsteroidsLeft;
	struct Asteroid *selectedAsteroidsRight;

	double x = asteroids[centarIndex].X;
	
    struct FittingLineParameters fittingLineLeft;
    struct FittingLineParameters fittingLineRight;
	
    //ubaceno radi iscrtavanja u graf
    printToFile("Levi.txt", asteroids, centarIndex);
    printToFile("Desni.txt", asteroids+centarIndex, numOfAsteroids-centarIndex);

	numOfBinsLeft = centarIndex/asteroidsPerBin;
	selectedAsteroidsLeft = (struct Asteroid*)malloc((int)(numOfBinsLeft+1)*sizeof(struct Asteroid));
	
	numOfBinsRight = (numOfAsteroids-centarIndex)/asteroidsPerBin;
	selectedAsteroidsRight = (struct Asteroid*)malloc((int)(numOfBinsRight+1)*sizeof(struct Asteroid));
	
    printf("numOfBinsLeft = %d\n", numOfBinsLeft);
    leftFit(asteroids, numOfAsteroids, centarIndex, &fittingLineLeft, selectedAsteroidsLeft);

    printf("numNumOfBinsRight = %d\n", numOfBinsRight);
    rightFit(asteroids+centarIndex, numOfAsteroids, centarIndex, &fittingLineRight, selectedAsteroidsRight);
    
    int ageLeft, ageErrorLeft, ageRight, ageErrorRight;
    char path[50];
	
	calculateAge(fittingLineLeft, x, &ageLeft, &ageErrorLeft);

    calculateAge(fittingLineRight, x, &ageRight, &ageErrorRight);

	strcpy(path, GNUPLOTPATH);
	strcat(path, " -persistent");
	
     FILE * gnuplotPipe = popen(path, "w");
     fprintf(gnuplotPipe, "set title \"Age left: %d +/- %d myr              Age right: %d +/- %d myr\"; \
			   set xrange [%f:%f]; \
			   set yrange [%f:%f]; \
			   plot \"Desni.txt\" using 1:2 title \"Right side\", \
				\"Levi.txt\" using 1:2 title \"Left side\", \
				\"IzabraniDesni.txt\" using 1:2 title \"Izabrani Right side\", \
				\"IzabraniLevi.txt\" using 1:2 title \"Izabrani Left side\", \
				%f%s%f * x, %f%s%f * x\n", ageLeft, ageErrorLeft, ageRight, ageErrorRight,
							  2.2, 2.5, 0.0, 1.0,
							  fittingLineLeft.c0, (fittingLineLeft.c1>0 ? "+" : ""), fittingLineLeft.c1,
							  fittingLineRight.c0, (fittingLineRight.c1>0 ? "+" : ""), fittingLineRight.c1);
							  
	pclose(gnuplotPipe);
	remove("Levi.txt");
	remove("Desni.txt");
	remove("Asteroidi.txt");
	remove("IzabraniLevi.txt");
	remove("IzabraniDesni.txt");

}


bool togetherFitting(struct Asteroid *asteroids, int numOfAsteroids, int centarIndex)
{
	int i;
	double k, y;
    int num_of_bins_left, num_of_bins_right;
	
	numOfBinsLeft = centarIndex/asteroidsPerBin;
	numOfBinsRight = (numOfAsteroids-centarIndex)/asteroidsPerBin;
	
    struct Asteroid *selectedAsteroidsLeft = (struct Asteroid*)malloc((int)(numOfBinsLeft+1)*sizeof(struct Asteroid));
	struct Asteroid *selectedAsteroidsRight = (struct Asteroid*)malloc((int)(numOfBinsRight+1)*sizeof(struct Asteroid));
	struct Asteroid *selectedAsteroidsLeft2 = (struct Asteroid*)malloc((int)(numOfBinsLeft+1)*sizeof(struct Asteroid));	
	struct Asteroid *selectedAsteroidsRight2 = (struct Asteroid*)malloc((int)(numOfBinsRight+1)*sizeof(struct Asteroid));	

    struct FittingLineParameters fittingLineLeft, fittingLineRight;

    //ubaceno radi iscrtavanja u graf
    printToFile("Levi.txt", asteroids, centarIndex);
    printToFile("Desni.txt", asteroids+centarIndex, numOfAsteroids-centarIndex);
		
    deterimineLeftBins(asteroids, centarIndex, numOfAsteroids, numOfBinsLeft, selectedAsteroidsLeft);
	printToFile("IzabraniLevi.txt", selectedAsteroidsLeft, numOfBinsLeft);
	for(i=0;i<numOfBinsLeft+1;i++)
	{
		selectedAsteroidsLeft2[i].X=selectedAsteroidsLeft[i].Y;
		selectedAsteroidsLeft2[i].Y=selectedAsteroidsLeft[i].X;
	}
			
    deterimineRightBins(asteroids+centarIndex, centarIndex, numOfAsteroids, numOfBinsRight, selectedAsteroidsRight);
	printToFile("IzabraniDesni.txt", selectedAsteroidsRight, numOfBinsRight);
	for(i=0;i<numOfBinsRight+1;i++)
	{
		selectedAsteroidsRight2[i].X=selectedAsteroidsRight[i].Y;
		selectedAsteroidsRight2[i].Y=selectedAsteroidsRight[i].X;

	}
	
	//toFitTogether(selectedAsteroidsLeft2, numOfBins, selectedAsteroidsRight2, numOfBins, asteroids[centarIndex].X, &k, &y);

	toFitTogether(selectedAsteroidsLeft, numOfBinsLeft, selectedAsteroidsRight, numOfBinsRight, asteroids[centarIndex].X, &k, &y);

	fittingLineRight.c0 = -k*asteroids[centarIndex].X+y;
	fittingLineRight.c1 = k;
	fittingLineRight.num_of_bins = numOfBinsRight;
	fittingLineRight.greska = sqrt(sumOfDeviations(selectedAsteroidsRight, fittingLineRight.c0, fittingLineRight.c1, numOfBinsRight+1))/(numOfBinsRight+1);
	
	
	fittingLineLeft.c0 = k*asteroids[centarIndex].X+y;
	fittingLineLeft.c1 = -k;
	fittingLineLeft.num_of_bins = numOfBinsLeft;

	fittingLineLeft.greska = sqrt(sumOfDeviations(selectedAsteroidsLeft, fittingLineLeft.c0, fittingLineLeft.c1, numOfBinsLeft+1))/(numOfBinsLeft+1);
	//fittingLineRight.greska = (fittingLineRight.greska + fittingLineLeft.greska)/2;
		

    int age, ageError;
	char path[50];

    calculateAge(fittingLineRight, asteroids[centarIndex].X, &age, &ageError);
	
	strcpy(path, GNUPLOTPATH);
	strcat(path, " -persistent");
	FILE * gnuplotPipe = popen(path, "w");
    fprintf(gnuplotPipe, "set title \"Age: %d +/- %d myr\"; \
			   set xrange [%f:%f]; \
			   set yrange [%f:%f]; \
			   plot \"Desni.txt\" using 1:2 title \"Right side\", \
				\"Levi.txt\" using 1:2 title \"Left side\", \
				\"IzabraniDesni.txt\" using 1:2 title \"Izabrani Right side\", \
				\"IzabraniLevi.txt\" using 1:2 title \"Izabrani Left side\", \
				%f%s%f * x, %f%s%f * x\n",
				age, ageError,
				2.2, 2.5, 0.0, 2.8,
				-k*asteroids[centarIndex].X+y, (k>0 ? "+" : ""), k,
				k*asteroids[centarIndex].X+y, (-k>0 ? "+" : ""), -k);

	pclose(gnuplotPipe);


    return true;
}


int parseArgs(int argc, char* argv[], struct Task* task)
{
    if(argc!=6)
    {
        printf("\n\nError: Invalid number of arguments.\nTry using:\nfit <filename> <numberOfAsteroids> <PickCenter|NonWeightedCenter|WeightedCenter> <Right|Left|RightAndLeft|CombinedWithCenter>.\n\n");
        
        printf("Age calculation of asteroid family will be done based on \ndiameters of asteroids and their semimajor axises.\n");
        printf("By estimating fitting lines of asteroid family, estimation\non how mouch would asteroid with diameter of 1km\nchange its semimajor axis in billion years will be done.\n\n");
        printf("For method to be sucessful, these fitting lines should form V shape.\n");
        printf("You can choose to count age in few diferent ways.\n");
        printf("Choose how center will be calculated using one of these:\n");
        printf("  PickCenter         - choose your own center (this could be x axis\n                       of biggest asteroid of family)\n");
        printf("  NonWeightedCenter  - calculate center as avrage of all semimajor\n                       axises in family\n");
        printf("  WeightedCenter     - calculate weighted center based on semimajos\n                       axises and diameters\n\n");
        printf("Choose how fitting will be calculated using one of these:\n");
        printf("  Left               - calculate age based only on left  part of V shape\n");
        printf("  Right              - calculate age based only on right part of V shape\n");
        printf("  RightAndLeft       - calculate age for both left and right part of\n                       V shape separatly\n");
        printf("  CombinedWithCenter - calculate age using center to fit both left \n                       and right side to name tilt\n");

        return -1;
    }
    else
    {
        char centering[50];
        char fitting[50];

		asteroidsPerBin = atoi(argv[5]);
        strcpy(task->filename, argv[1]);
        task->num_of_asteroids = atoi(argv[2]);
        
        strcpy(centering, argv[3]);
        if(strcmp(centering, "PickCenter")==0)
            task->centering = PickCenter;
        else if(strcmp(centering, "NonWeightedCenter")==0)
            task->centering = NonWeightedCenter;
        else if(strcmp(centering, "WeightedCenter")==0)
            task->centering = WeightedCenter;
        else
        {
            printf("Error: Invalid centering argument.\nfit <filename> <numberOfAsteroids> <PickCenter|NonWeightedCenter|WeightedCenter> <Right|Left|RightAndLeft|CombinedWithCenter>.\n\n");
            return -1;
        }        
        
        strcpy(fitting, argv[4]);
        if(strcmp(fitting, "Right")==0)
            task->fitting = Right;
        else if(strcmp(fitting, "Left")==0)
            task->fitting = Left;
        else if(strcmp(fitting, "RightAndLeft")==0)
            task->fitting = RightAndLeft;
        else if(strcmp(fitting, "CombinedWithCenter")==0)
            task->fitting = CombinedWithCenter;
        else
        {
            printf("\n\nError: Invalid fitting argument.\nfit <filename> <numberOfAsteroids> <PickCenter|NonWeightedCenter|WeightedCenter> <Right|Left|RightAndLeft|CombinedWithCenter>.\n\n");
            return -1;
        }
        
    }
    return 0;
}


int main(int argc, char* argv[])
{

    struct Task task;
    int i, centarIndex;
    double centar;
    char filename[50];
    struct Asteroid *asteroids = NULL;
    
    printf("\nWelcome!\n");
    printf("-------------------------------\n");
    printf("This software is designed to calculate age of\nasteroid families using Jarkovski effect.\n");
    if(parseArgs(argc, argv, &task)!=0)
    {
        printf("\n\nAgruments parse failed.\n");
        printf("Ending program. Bye...\n");
        return 0;        
    }

    asteroids = (struct Asteroid*)malloc(numOfAsteroids*sizeof(struct Asteroid));
    if(parceFamilyFromFile(asteroids, argv[1], &numOfAsteroids) < 0)
    {
        printf("File parse failed.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }
    qsort(asteroids, numOfAsteroids, sizeof(struct Asteroid), sortByX);

    //find center of family
    char centerStr[50];
    switch (task.centering)
    {
        case PickCenter:
                printf("Enter x axis of center: ");
                scanf("%s", (char*)&centerStr);
                //centar = atof(centerStr);
                centarIndex = findNextToPickedCenter(asteroids, atof(centerStr), numOfAsteroids);
                break;
        case NonWeightedCenter:
                centarIndex = findNonWeightedCenter(asteroids, &centar, numOfAsteroids);
                break;
        case WeightedCenter:
                centarIndex = findWeightedCenter(asteroids, &centar, numOfAsteroids);
                break;
        default:
                printf("\nError: Centering not determined.\n");
                printf("Ending program. Bye...\n");
                return 0;
                //break;
    }

    if(centarIndex<0)
    {
        printf("\nError: Center not determined.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }
    printf("-------------------------------\n");
    printf("Center of a family: %f\ncentarIndex %d\n", centar, centarIndex);
    // end of finding center of family

    //choose witch way age calculating will be done
    switch (task.fitting)
    {
        case Right:
                if(task.centering = PickCenter)
                {
                    // o ovome mora da se razmisli zato sto nisam sigurna kada imam centar i racunam za desnu stranu, sta bi mi bio ekvivalent preseka dve prave???
                    printf("To be done ... exit\n");
                    return 0;  
                }
				printf("To be done \nDon't know how to do this yet so I will just count left and right...\n");
                fittingRightAndLeft(asteroids, numOfAsteroids, centarIndex);
                break;
        case Left:
                if(task.centering = PickCenter)
                {
                    // o ovome mora da se razmisli zato sto nisam sigurna kada imam centar i racunam za desnu stranu, sta bi mi bio ekvivalent preseka dve prave???
                    printf("To be done ... exit\n");
                    return 0;  
                }
				printf("To be done \nDon't know how to do this yet so I will just count left and right...\n");
                fittingRightAndLeft(asteroids, numOfAsteroids, centarIndex);
                break;
        case RightAndLeft:
				printf("Counting left and right!!!!\n");
                fittingRightAndLeft(asteroids, numOfAsteroids, centarIndex);
                break;
        case CombinedWithCenter:
				if(!togetherFitting(asteroids, numOfAsteroids, centarIndex))
                {
                     printf("ERROR[CombinedWithCenter]: MAX TOLERATED ERROR VIOLATED!!!\n");
                    return 0;
                }
                //fittingRightAndLeft(asteroids, numOfAsteroids, centarIndex);
                break;
        default:
                printf("\nError: Fitting not done.\n");
                printf("Ending program. Bye...\n");
                return 0;
                //break;
    }
    //end of age calculating

    printf("Success: Age calculated.\nEnding program. Bye...\n"); 

    return 0;

}
