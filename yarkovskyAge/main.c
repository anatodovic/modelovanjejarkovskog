#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_fit.h>

#define FIELD_SIZE 15
#define LINE_SIZE 100

#define DEST_FILE "Asteroidi.txt"

#ifdef linux
	#define GNUPLOTPATH "gnuplot"
#else
	#define GNUPLOTPATH "C:/MinGW/bin/gnuplot/bin/gnuplot"
#endif

int numOfBinsLeft = 12;
int numOfBinsRight = 12;

struct Asteroid
{
    double X;
    double Y;
    double errY;
};

struct FittingLineParameters
{
    double c0;
    double c1;
    double greska;
};

typedef enum
{
    PickCenter = 0,
    NonWeightedCenter,
    WeightedCenter
} CENTERING;

struct Task
{
    char filename[50];
    CENTERING centering;
};

int sortByY (const void * a, const void * b)
{
    return ( ((((struct Asteroid*)a))->Y >= (((struct Asteroid*)b)->Y)) ? 1 : -1);
}

int sortByX (const void * a, const void * b)
{
    return ( ((((struct Asteroid*)a))->X >= (((struct Asteroid*)b)->X)) ? 1 : -1);
}

/*
Function: parseFamilyFromFile
Description: This function will parse asteroids from jablunka.txt
-
1. broj (oznaka) asteroida - label
2. apsolutna magnituda, H
3. velika poluosa, a - semi_major_axes
4. ekscentricitet, e - eccentricity
5. sinus nagiba, sin(i) - sin_i
6. prečnik, D - diametar
7. greška poluprečnika, sigma(D) - sigma_D

*/
int parceStartFamilyFromFile(struct Asteroid *asteroids, char *source_str, int *numOfAsteroids)
{
    FILE *source, *dest;

    char label[FIELD_SIZE],
         absolute_magnitude[FIELD_SIZE],
         semi_major_axes[FIELD_SIZE],
         eccentricity[FIELD_SIZE],
         sin_i[FIELD_SIZE],
         D[FIELD_SIZE],
         sigma_D[FIELD_SIZE];
    char line[LINE_SIZE];
    int  i=0;

    source = fopen (source_str, "r");
    dest = fopen(DEST_FILE, "w+");

    while (fgets(line, sizeof(line), source))
    {
        double x, y;
        sscanf(line, "%s %s %s %s %s %s %s", label, absolute_magnitude, semi_major_axes, eccentricity, sin_i, D, sigma_D);

        x = atof(semi_major_axes);
        y = 1/atof(D);
        asteroids[i].X=x;
        asteroids[i].Y=y;
        // D + sigma_D
        //sigma_1D = 1/(D+sigma_D)-1/D
        asteroids[i].errY=fabs(1/(atof(D)+atof(sigma_D))-1/atof(D));
        //printf("asteroids[i].errY= %f\n", asteroids[i].errY);
        fprintf(dest, "%f, %f\n", x, y);
        i++;
    }

    if(i<(*numOfAsteroids) || (*numOfAsteroids)==0)
    {
        printf("warnning: number of asteroids set based on num of asteroids in file\n          ");
        (*numOfAsteroids) = i;
        printf("number of asteroids: %d\n", (*numOfAsteroids));
    }

    fclose(source);
    fclose(dest);
    return 0;
}

void printToFile(const char *srcDest, struct Asteroid *asteroids, int numOfAsteroids)
{
    int i;
    FILE *dest = fopen(srcDest, "w+");

    for(i=0; i<numOfAsteroids; i++)
    {
        fprintf(dest,"%f, %f\n", asteroids[i].X, asteroids[i].Y);
    }

    fclose(dest);
}

void printFittingLine(struct FittingLineParameters *fittingLine)
{
    printf("-------------------------------\n");
    printf("best fit:  y=%f%s%f * x\n",fittingLine->c0, (fittingLine->c1>0 ? "+" : ""), fittingLine->c1);
    printf("error: %f\n", fittingLine->greska);
    printf("-------------------------------\n");
}

int findNonWeightedCenter(struct Asteroid *asteroids, double *centar, int numOfAsteroids)
{
    int i;
    double sum = 0.0;

    for(i=0; i<numOfAsteroids; i++)
    {
        sum = sum + asteroids[i].X;
    }

    *centar = sum/numOfAsteroids;
    for(i=0; i<numOfAsteroids; i++)
    {
        if(asteroids[i].X < *centar && asteroids[i+1].X > *centar)
        {
            //printf("NonWeightedCenter = %f [Index = %d]\n", *centar, i);
            return i;
        }
    }
    return -1;
}

int findWeightedCenter(struct Asteroid *asteroids, double *centar, int numOfAsteroids)
{
    int i;
    double sum = 0.0;
    double sumD = 0.0;

    for(i=0; i<numOfAsteroids; i++)
    {
        sumD = sumD + pow(1/asteroids[i].Y, 3);
    }

    for(i=0; i<numOfAsteroids; i++)
    {
        sum = sum + asteroids[i].X/pow(asteroids[i].Y, 3);
    }

    *centar = sum/sumD;
    for(i=0; i<numOfAsteroids; i++)
    {
        if(asteroids[i].X < *centar && asteroids[i+1].X > *centar)
        {
            //printf("WeightedCenter = %f [Index = %d]\n", *centar, i);
            return i;
        }
    }
    return -1;
}

int findNextToPickedCenter(struct Asteroid *asteroids, double center, int numOfAsteroids)
{
    int i;
    // to find Jablunka, we will find asteroid with smalest value of y (this will be the biggest asteroid 1/D !!!
    //qsort by Y axis to devide in bins
    //qsort(asteroids, numOfAsteroids, sizeof(struct Asteroid), sortByY);

    //qsort by X axis
    qsort(asteroids, (size_t)numOfAsteroids, sizeof(struct Asteroid), sortByX);

    //find Jablunka's index
    for(i=0; i<numOfAsteroids; i++)
    {
        if(center > asteroids[i].X && center < asteroids[i+1].X)
        {
            printf("Centar=%f\nCentarIndex=%d\n", center, i);
            return i;
        }
    }

    printf("Error: Center not found. Chech if center you entered is within range of asteroids\nx axis: [%f, %f]\n", asteroids[0].X, asteroids[numOfAsteroids-1].X);
    qsort(asteroids, (size_t)numOfAsteroids, sizeof(struct Asteroid), sortByY);

    return -1;
}

void findMinIndex(struct Asteroid *asteroids, int start, int size, int *min, int *minSec)
{
    int i;
    *min=start;
    for(i=start; i<start+size; i++)
    {
        //printf("min asteroids[%d].X %f %f\n",*min, asteroids[*min].X, asteroids[*min].Y);
        if(asteroids[*min].X > asteroids[i].X)
	    {
	        *minSec = *min;
            *min=i;
	    }
    }
}

void findMaxIndex(struct Asteroid *asteroids, int start, int size, int *max, int *maxSec)
{
    int i;
    *max=start;
    for(i=start; i<start + size; i++)
    {
        if(asteroids[*max].X < asteroids[i].X)
	    {
		    *maxSec = *max;
		    *max=i;
	    }
    }
}

void deterimineLeftBins(struct Asteroid *asteroids, int numOfAsteroidsLeft, int num_of_bins, struct Asteroid *selectedAsteroids)
{
    int i = 0;
    struct Asteroid peakAsteroid;
    int numberOfAsteroidsInBin = abs(numOfAsteroidsLeft)/num_of_bins;
    qsort(asteroids, (size_t)(numOfAsteroidsLeft), sizeof(struct Asteroid), sortByY);

    peakAsteroid.X = asteroids[0].X;
    peakAsteroid.Y = asteroids[0].Y;
    peakAsteroid.errY = asteroids[0].errY;

    printf("left %d %d %d\n", numOfAsteroidsLeft, num_of_bins, numberOfAsteroidsInBin);
    while(i<num_of_bins)
    {
        int min=0, minSec=0;
        double minErr, minSecErr;

        //printf("i = %d, numberOfAsteroidsInBin = %d\n", i, numberOfAsteroidsInBin);
	    findMinIndex(asteroids, i*numberOfAsteroidsInBin, numberOfAsteroidsInBin, &min, &minSec);

        //printf("sasteroids %f %f\n", asteroids[min].X, asteroids[min].Y);
        //printf("min %d\n", min);
        selectedAsteroids[i].X = asteroids[min].X;
        selectedAsteroids[i].Y = asteroids[min].Y;
        selectedAsteroids[i].errY = asteroids[min].errY;

        printf("left selectedAsteroids %d %f %f %f\n", i, selectedAsteroids[i].X, selectedAsteroids[i].Y, selectedAsteroids[i].errY);
        i++;

    }

    selectedAsteroids[i].X = peakAsteroid.X;
    selectedAsteroids[i].Y = peakAsteroid.Y;
    selectedAsteroids[i].errY = peakAsteroid.errY;

    printf("peak %f %f\n", peakAsteroid.X, peakAsteroid.Y);
}


void deterimineRightBins(struct Asteroid *asteroids, int numOfAsteroidsRight, int num_of_bins, struct Asteroid *selectedAsteroids)
{
    int i = 0;
    struct Asteroid peakAsteroid;
    int numberOfAsteroidsInBin = abs(numOfAsteroidsRight)/num_of_bins;
    qsort(asteroids, (size_t)(numOfAsteroidsRight), sizeof(struct Asteroid), sortByY);

    peakAsteroid.X = asteroids[0].X;
    peakAsteroid.Y = asteroids[0].Y;
    peakAsteroid.errY = asteroids[0].errY;

    while(i<num_of_bins) {
        int max = 0, maxSec = 0;
        double maxErr;
        double maxSecErr;

        findMaxIndex(asteroids, i * numberOfAsteroidsInBin, numberOfAsteroidsInBin, &max, &maxSec);
        selectedAsteroids[i].X = asteroids[max].X;
        selectedAsteroids[i].Y = asteroids[max].Y;
        selectedAsteroids[i].errY = asteroids[max].errY;
        i++;
    }

    selectedAsteroids[i].X = peakAsteroid.X;
    selectedAsteroids[i].Y = peakAsteroid.Y;
    selectedAsteroids[i].errY = peakAsteroid.errY;
}

int leftFit(struct Asteroid *asteroids, int numOfAsteroidsLeft, struct FittingLineParameters *fittingLineLeft,
            double *c0, double *c1, double *cov00, double *cov01, double *cov11, double *chisq, char* path)
{
    int i;
    double y_err;
    struct Asteroid *selectedAsteroids = (struct Asteroid*)malloc((numOfBinsLeft+1)*sizeof(struct Asteroid));

    double *X = (double*)malloc((numOfBinsLeft+1)*sizeof(double));
    double *Y = (double*)malloc((numOfBinsLeft+1)*sizeof(double));
    double *errY = (double*)malloc((numOfBinsLeft+1)*sizeof(double));

    deterimineLeftBins(asteroids, numOfAsteroidsLeft, numOfBinsLeft, selectedAsteroids);

	for(i=0;i<numOfBinsLeft+1;i++)
	{
        X[i]=selectedAsteroids[i].X;
		Y[i]=selectedAsteroids[i].Y;
        errY[i]=selectedAsteroids[i].errY;

        //printf("KLKJJ %f %f %f\n", selectedAsteroids[i].X, selectedAsteroids[i].Y, selectedAsteroids[i].errY);
    }
    //gsl_fit_wlinear (X, 1, errY, 1, Y, 1, (const size_t)numOfBinsLeft+1, c0, c1, cov00, cov01, cov11, chisq);
	gsl_fit_linear(X, 1, Y, 1, (const size_t)numOfBinsLeft+1, c0, c1, cov00, cov01, cov11, chisq);

    gsl_fit_linear_est((1-*c0)/(*c1), *c0, *c1, *cov00, *cov01, *cov11, Y, &y_err);
	fittingLineLeft->c0 = *c0;
	fittingLineLeft->c1 = *c1;
	fittingLineLeft->greska = y_err;

	printToFile(path, selectedAsteroids, numOfBinsLeft+1);
	printFittingLine(fittingLineLeft);

	free(X);
	free(Y);
    free(selectedAsteroids);

    return 0;
}

int rightFit(struct Asteroid *asteroids, int numOfAsteroidsRight, struct FittingLineParameters *fittingLineRight,
             double *c0, double *c1, double *cov00, double *cov01, double *cov11, double *chisq, char* path)
{
	int i;
    double y_err;
    struct Asteroid *selectedAsteroids = (struct Asteroid*)malloc((numOfBinsRight+1)*sizeof(struct Asteroid));

    double *X = (double*)malloc((numOfBinsRight+1)*sizeof(double));
    double *Y = (double*)malloc((numOfBinsRight+1)*sizeof(double));
    double *errY = (double*)malloc((numOfBinsLeft+1)*sizeof(double));

	deterimineRightBins(asteroids, numOfAsteroidsRight, numOfBinsRight, selectedAsteroids);

	for(i=0;i<numOfBinsRight+1;i++)
	{
		X[i]=selectedAsteroids[i].X;
		Y[i]=selectedAsteroids[i].Y;
        errY[i]=selectedAsteroids[i].errY;
	}
    //gsl_fit_wlinear (X, 1, errY, 1, Y, 1, (const size_t)numOfBinsLeft+1, c0, c1, cov00, cov01, cov11, chisq);
    gsl_fit_linear(X, 1, Y, 1, (const size_t)numOfBinsRight+1, c0, c1, cov00, cov01, cov11, chisq);

    fittingLineRight->c0 = *c0;
	fittingLineRight->c1 = *c1;

	gsl_fit_linear_est((1-*c0)/(*c1), *c0, *c1, *cov00, *cov01, *cov11, Y, &y_err);
	fittingLineRight->greska = y_err;
	printToFile(path, selectedAsteroids, numOfBinsRight+1);
    printFittingLine(fittingLineRight);

	free(X);
	free(Y);
    free(selectedAsteroids);
    return 0;
}


void calculateAge(struct FittingLineParameters fittingLineFinal, double center, int *age, int *ageError)
{
     // vrednost a (tj.x) za 1/D tj. y=1/D; koristimo levu pravu
     // ako znamo da Jarkovski promeni veliku poluosu asteroida precnika 1km za 0.0005AJ, onda:
     // promena velike poluose   ----  x godina
     // 0.0005                   ----  1 milion godina

     double a_endDesno = center;
     double a_endLevo = 1/fittingLineFinal.c1 - fittingLineFinal.c0/fittingLineFinal.c1;

     *age = (int)(fabs(a_endLevo-a_endDesno)/0.00053);
     *ageError = (int)(fittingLineFinal.greska*(*age));

     printf("Age of a family: %d +/- %f millions of years\n", *age, fittingLineFinal.greska);
     printf("-------------------------------\n");


}

int parseArgs(int argc, char* argv[], struct Task* task)
{
    if(argc!=5)
    {
        printf("\n\nError: Invalid number of arguments.\nTry using:\nfit <filename> <numberOfAsteroids> <PickCenter|NonWeightedCenter|WeightedCenter> <Right|Left|RightAndLeft|CombinedWithCenter>.\n\n");

        printf("Age calculation of asteroid family will be done based on \ndiameters of asteroids and their semimajor axises.\n");
        printf("By estimating fitting lines of asteroid family, estimation\non how mouch would asteroid with diameter of 1km\nchange its semimajor axis in billion years will be done.\n\n");
        printf("For method to be sucessful, these fitting lines should form V shape.\n");
        printf("You can choose to count age in few diferent ways.\n");
        printf("Choose how center will be calculated using one of these:\n");
        printf("  PickCenter         - choose your own center (this could be x axis\n                       of biggest asteroid of family)\n");
        printf("  NonWeightedCenter  - calculate center as avrage of all semimajor\n                       axises in family\n");
        printf("  WeightedCenter     - calculate weighted center based on semimajos\n                       axises and diameters\n\n");
        printf("Choose how fitting will be calculated using one of these:\n");
        printf("  Left               - calculate age based only on left  part of V shape\n");
        printf("  Right              - calculate age based only on right part of V shape\n");
        printf("  RightAndLeft       - calculate age for both left and right part of\n                       V shape separatly\n");
        printf("  CombinedWithCenter - calculate age using center to fit both left \n                       and right side to name tilt\n");

        return -1;
    }
    else
    {
        strcpy(task->filename, argv[1]);

        if(strcmp(argv[3], "PickCenter")==0)
            task->centering = PickCenter;
        else if(strcmp(argv[3], "NonWeightedCenter")==0)
            task->centering = NonWeightedCenter;
        else if(strcmp(argv[3], "WeightedCenter")==0)
            task->centering = WeightedCenter;
        else
        {
            printf("Error: Invalid centering argument.\nfit <filename> <numberOfAsteroids> <PickCenter|NonWeightedCenter|WeightedCenter> <Right|Left|RightAndLeft|CombinedWithCenter>.\n\n");
            return -1;
        }

    }
    return 0;
}

void endPlot(char *plotArg, int end) {
    if (!end) {
        strcat(plotArg, ", ");
    } else {
        strcat(plotArg, "\n");
    }
}
void addPointsToPlot(char *plotArg, char *name, int end)
{
    char izabraniCorrectedLeviStr[100];
    sprintf(izabraniCorrectedLeviStr, "\"%s\" using 1:2 title \"%s\"", name, name);
    strcat(plotArg, izabraniCorrectedLeviStr);
    endPlot(plotArg, end);
}

void addLineToPlot(char *plotArg, struct FittingLineParameters line, int end) {
    char prava[100];
    sprintf(prava, "%f%s%f * x", line.c0, (line.c1 > 0 ? "+" : ""), line.c1);
    strcat(plotArg, prava);
    endPlot(plotArg, end);
}

void determineAge(struct FittingLineParameters fittingLineLeftCorrected,
                  struct FittingLineParameters fittingLineRightCorrected,
                  struct FittingLineParameters fittingLineLeft,
                  struct FittingLineParameters fittingLineRight,
                  char* pathLeft, char* pathLeftCorrected,
                  char* pathRight, char* pathRightCorrected,
                  double x)
{
    char path[50];
    int ageLeft, ageErrorLeft, ageRight, ageErrorRight;

calculateAge(fittingLineLeftCorrected, x, &ageLeft, &ageErrorLeft);
calculateAge(fittingLineRightCorrected, x, &ageRight, &ageErrorRight);

strcpy(path, GNUPLOTPATH);
strcat(path, " -persistent");

char plotArg[1000], ageStr[100], rangeStr[100], plotStr[100];
strcpy(plotArg, "set title ");

sprintf(ageStr, "\"Starost levo: %d +/- %d myr           Starost desno: %d +/- %d myr\"; ", ageLeft,
ageErrorLeft, ageRight, ageErrorRight);
strcat(plotArg, ageStr);
sprintf(rangeStr, "set xrange [%f:%f]; set yrange [%f:%f]; \n", 2.2, 2.5, 0.0, 1.0);
strcat(plotArg, rangeStr);
sprintf(plotStr, "plot ");
strcat(plotArg, plotStr);

addPointsToPlot(plotArg, "Levi.txt", 0);
addPointsToPlot(plotArg, "Desni.txt", 0);
addPointsToPlot(plotArg, pathLeft, 0);
addPointsToPlot(plotArg, pathRight, 0);
addPointsToPlot(plotArg, pathLeftCorrected, 0);
addPointsToPlot(plotArg, pathRightCorrected, 0);

addLineToPlot(plotArg, fittingLineLeft, 0);
addLineToPlot(plotArg, fittingLineRight, 0);
addLineToPlot(plotArg, fittingLineLeftCorrected, 0);
addLineToPlot(plotArg, fittingLineRightCorrected, 1);

FILE *gnuplotPipe = popen(path, "w");
fprintf(gnuplotPipe, plotArg);

pclose(gnuplotPipe);
//remove("Levi.txt");
//remove("Desni.txt");
remove("Asteroidi.txt");
remove("IzabraniLevi.txt");
remove("IzabraniDesni.txt");

}

void rightAndLeft(struct Asteroid* asteroidsLeft, int numOfAsteroidsLeft, char* pathLeft, char* pathLeftCorrected, struct Asteroid* asteroidsRight, int numOfAsteroidsRight, char* pathRight, char* pathRightCorrected, double x)
{
    struct FittingLineParameters fittingLineLeft;
    struct FittingLineParameters fittingLineRight;

    struct FittingLineParameters fittingLineLeftCorrected;
    struct FittingLineParameters fittingLineRightCorrected;

    double c0, c1, cov00, cov01, cov11, chisq;
    double YY;
    double minErr;


    leftFit(asteroidsLeft, numOfAsteroidsLeft, &fittingLineLeft, &c0, &c1, &cov00, &cov01, &cov11, &chisq,
            pathLeft);
    printf("numOfAsteroidsLeft %d\n", numOfAsteroidsLeft);
    int last = numOfAsteroidsLeft;
    for (int j = 1; j < numOfAsteroidsLeft; j++) {
        gsl_fit_linear_est(asteroidsLeft[j].X, c0, c1, cov00, cov01, cov11, &YY, &minErr);
        if (minErr > 0.01 && asteroidsLeft[j].Y < c0 + c1 * asteroidsLeft[j].X) {
            printf("exclude left %f %f\n", asteroidsLeft[j].X, asteroidsLeft[j].Y);
            asteroidsLeft[j].X = asteroidsLeft[last - 1].X;
            asteroidsLeft[j].Y = asteroidsLeft[last - 1].Y;
            asteroidsLeft[j].errY = asteroidsLeft[last - 1].errY;
            last--;
        }
    }
    numOfAsteroidsLeft = last;
    printf("numOfAsteroidsLeft %d\n", numOfAsteroidsLeft);
    //remove("IzabraniLevi.txt");
    leftFit(asteroidsLeft, numOfAsteroidsLeft, &fittingLineLeftCorrected, &c0, &c1, &cov00, &cov01, &cov11, &chisq,
            pathLeftCorrected);

    /*    */
    rightFit(asteroidsRight, numOfAsteroidsRight, &fittingLineRight, &c0, &c1, &cov00, &cov01, &cov11, &chisq,
             pathRight);
    last = numOfAsteroidsRight;
    for (int j = 1; j < numOfAsteroidsRight; j++) {
        gsl_fit_linear_est(asteroidsRight[j].X, c0, c1, cov00, cov01, cov11, &YY, &minErr);

        if (minErr > 0.01 && asteroidsRight[j].Y < c0 + c1 * asteroidsRight[j].X) {
            //printf("exclude right %f %f\n", asteroidsRight[j].X, asteroidsRight[j].Y);
            asteroidsRight[j].X = asteroidsRight[last - 1].X;
            asteroidsRight[j].Y = asteroidsRight[last - 1].Y;
            asteroidsRight[j].errY = asteroidsRight[last - 1].errY;

            last--;
        }
    }
    numOfAsteroidsRight = last;
    //printf("numOfAsteroidsRight %d\n", numOfAsteroidsRight);

    rightFit(asteroidsRight, numOfAsteroidsRight, &fittingLineRightCorrected, &c0, &c1, &cov00, &cov01, &cov11,
             &chisq, pathRightCorrected);


    determineAge(fittingLineLeftCorrected,
                 fittingLineRightCorrected,
                 fittingLineLeft,
                 fittingLineRight,
                 "IzabraniLevi.txt","IzabraniCorrectedLevi.txt",
                 "IzabraniDesni.txt","IzabraniCorrectedDesni.txt",
                 x);
}


int main(int argc, char* argv[])
{
    int numOfAsteroids = 7430;
    struct Task task;
    int centarIndex;
    double centar=0;
    struct Asteroid *asteroids = NULL;

    printf("\nWelcome!\n");
    printf("-------------------------------\n");
    printf("This software is designed to calculate age of\nasteroid families using Jarkovski effect.\n");
    if(parseArgs(argc, argv, &task)!=0)
    {
        printf("\n\nAgruments parse failed.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }

    asteroids = (struct Asteroid*)malloc(7430*sizeof(struct Asteroid));
    if(parceStartFamilyFromFile(asteroids, argv[1], &numOfAsteroids) < 0)
    {
        printf("File parse failed.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }

    qsort(asteroids, (size_t)numOfAsteroids, sizeof(struct Asteroid), sortByX);

    //find center of family
    char centerStr[50];
    switch (task.centering)
    {
        case PickCenter:
                printf("Enter x axis of center: ");
                scanf("%s", (char*)&centerStr);
                centarIndex = findNextToPickedCenter(asteroids, atof(centerStr), numOfAsteroids);
                break;
        case NonWeightedCenter:
                centarIndex = findNonWeightedCenter(asteroids, &centar, numOfAsteroids);
                break;
        case WeightedCenter:
                centarIndex = findWeightedCenter(asteroids, &centar, numOfAsteroids);
                break;
        default:
                printf("\nError: Centering not determined.\n");
                printf("Ending program. Bye...\n");
                return 0;
                //break;
    }

    if(centarIndex<0 || centar==0)
    {
        printf("\nError: Center not determined.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }
    printf("-------------------------------\n");
    printf("Center of a family: %f\ncentarIndex %d\n", centar, centarIndex);
    // end of finding center of family

    double x = asteroids[centarIndex].X;

    int numOfAsteroidsLeft = centarIndex;
    int numOfAsteroidsRight = numOfAsteroids-centarIndex;

    struct Asteroid* asteroidsLeft = (struct Asteroid*)malloc((numOfAsteroidsLeft)*sizeof(struct Asteroid));
    struct Asteroid* asteroidsRight = (struct Asteroid*)malloc((numOfAsteroidsRight)*sizeof(struct Asteroid));

    for(int j=0; j<numOfAsteroidsLeft; j++)
    {
         asteroidsLeft[j].X = asteroids[j].X;
         asteroidsLeft[j].Y = asteroids[j].Y;
         asteroidsLeft[j].errY = asteroids[j].errY;
    }

    printf("numOfAsteroidsRight %d\n", numOfAsteroidsRight);
    for(int j=0; j<numOfAsteroidsRight; j++)
    {
        asteroidsRight[j].X=asteroids[j+centarIndex-1].X;
        asteroidsRight[j].Y=asteroids[j+centarIndex-1].Y;
        asteroidsRight[j].errY=asteroids[j+centarIndex-1].errY;
    }

    printToFile("Levi.txt", asteroidsLeft, numOfAsteroidsLeft);
    printToFile("Desni.txt", asteroidsRight, numOfAsteroidsRight);

    rightAndLeft(asteroidsLeft, numOfAsteroidsLeft, "IzabraniLevi.txt", "IzabraniCorrectedLevi.txt", asteroidsRight, numOfAsteroidsRight, "IzabraniDesni.txt", "IzabraniCorrectedDesni.txt", x);

    //together fitting
    for(int j=0; j<numOfAsteroidsLeft; j++)
    {
        asteroids[j].X = asteroidsLeft[j].X+2*fabs(x-asteroidsLeft[j].X);
        printf("LEFT %f %f %f\n", asteroids[j].X, asteroids[j].Y, asteroids[j].errY);
    }

    for(int j=numOfAsteroidsLeft; j<numOfAsteroidsLeft+numOfAsteroidsRight; j++)
    {
        asteroids[j].X=asteroidsRight[j-numOfAsteroidsLeft].X;
        asteroids[j].Y=asteroidsRight[j-numOfAsteroidsLeft].Y;
        asteroids[j].errY=asteroidsRight[j-numOfAsteroidsLeft].errY;

        printf("RIGHT %f %f %f\n", asteroids[j].X, asteroids[j].Y, asteroids[j].errY);
    }

    ///////////////

    struct FittingLineParameters fittingLine;
    struct FittingLineParameters fittingLine2;

    struct FittingLineParameters fittingLineCorrected;
    struct FittingLineParameters fittingLine2Corrected;

    double c0, c1, cov00, cov01, cov11, chisq;
    double YY;
    double minErr;

    rightFit(asteroids, numOfAsteroidsLeft+numOfAsteroidsRight, &fittingLine, &c0, &c1, &cov00, &cov01, &cov11, &chisq,
            "Izabrani.txt");
    printf("numOfAsteroidsLeft +numOfAsteroidsRight %d\n", numOfAsteroidsLeft+numOfAsteroidsRight);
    int last = numOfAsteroidsLeft+numOfAsteroidsRight;
    for (int j = 1; j < numOfAsteroidsLeft+numOfAsteroidsRight; j++) {
        gsl_fit_linear_est(asteroids[j].X, c0, c1, cov00, cov01, cov11, &YY, &minErr);
        if (minErr > 0.01 && asteroids[j].Y < c0 + c1 * asteroids[j].X) {
            printf("exclude left %f %f\n", asteroids[j].X, asteroids[j].Y);
            asteroids[j].X = asteroids[last - 1].X;
            asteroids[j].Y = asteroids[last - 1].Y;
            asteroids[j].errY = asteroids[last - 1].errY;
            last--;
        }
    }
    //numOfAsteroidsLeft = last;
    //printf("numOfAsteroidsLeft %d\n", numOfAsteroidsLeft);
    //remove("IzabraniLevi.txt");
    rightFit(asteroids, last, &fittingLineCorrected, &c0, &c1, &cov00, &cov01, &cov11, &chisq,
            "IzabraniCorrected.txt");

    fittingLine2.greska = fittingLine.greska;
    fittingLine2.c0 = -fittingLine.c0;
    fittingLine2.c1 = -fittingLine.c1;


    fittingLine2Corrected.greska = fittingLineCorrected.greska;
    fittingLine2Corrected.c0 = -fittingLineCorrected.c0;
    fittingLine2Corrected.c1 = -fittingLineCorrected.c1;

    determineAge(fittingLine2Corrected, fittingLineCorrected, fittingLine2, fittingLine,
                 "Izabrani.txt","IzabraniCorrected.txt",
                 "Izabrani.txt","IzabraniCorrected.txt",
                 x);
    ////////////////
    printf("Success: Age calculated.\nEnding program. Bye...\n");



    return 0;
}

