cmake_minimum_required(VERSION 3.6)
project(yarkovskyAge)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 gsl m")

include_directories(-I/usr/local/include)

set(SOURCE_FILES main.c)
add_executable(yarkovskyAge ${SOURCE_FILES})

target_link_libraries(yarkovskyAge gsl)
target_link_libraries(yarkovskyAge gslcblas)