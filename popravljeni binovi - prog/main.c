#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_fit.h>

#define FIELD_SIZE 15
#define LINE_SIZE 100

#define DEST_FILE "Asteroidi.txt"

#ifdef linux
	#define GNUPLOTPATH "gnuplot"
#else
	#define GNUPLOTPATH "C:/MinGW/bin/gnuplot/bin/gnuplot"
#endif

int numOfBinsLeft = 10;
int numOfBinsRight = 10;

struct Asteroid
{
    double X;
    double Y;
    double errY;
};

struct FittingLineParameters
{
    double c0;
    double c1;
    double greska;
};


int numOfAsteroids = 7430;

typedef enum
{
    PickCenter = 0,
    NonWeightedCenter,
    WeightedCenter
} CENTERING;

//fit jablunka.list 290 NonWeightedCenter Left
struct Task
{
    char filename[50];
    CENTERING centering;
};

int sortByY (const void * a, const void * b)
{
    return ( ((((struct Asteroid*)a))->Y >= (((struct Asteroid*)b)->Y)) ? 1 : -1);
}

int sortByX (const void * a, const void * b)
{
    return ( ((((struct Asteroid*)a))->X >= (((struct Asteroid*)b)->X)) ? 1 : -1);
}

/*
Function: parseFamilyFromFile
Description: This function will parse asteroids from jablunka.txt
-
1. broj (oznaka) asteroida - label
2. apsolutna magnituda, H
3. velika poluosa, a - semi_major_axes
4. ekscentricitet, e - eccentricity
5. sinus nagiba, sin(i) - sin_i
6. prečnik, D - diametar
7. greška poluprečnika, sigma(D) - sigma_D

*/
int parceFamilyFromFile(struct Asteroid *asteroids, char *source_str, int *numOfAsteroids)
{
    FILE *source, *dest;

    char label[FIELD_SIZE],
         absolute_magnitude[FIELD_SIZE],
         semi_major_axes[FIELD_SIZE],
         eccentricity[FIELD_SIZE],
         sin_i[FIELD_SIZE],
         diametar[FIELD_SIZE],
         sigma_D[FIELD_SIZE];
    char line[LINE_SIZE];
    int  i=0;

    source = fopen (source_str, "r");
    dest = fopen(DEST_FILE, "w+");

    while (fgets(line, sizeof(line), source))
    {
        double x, y;
        sscanf(line, "%s %s %s %s %s %s %s", label, absolute_magnitude, semi_major_axes, eccentricity, sin_i, diametar, sigma_D);

        x = atof(semi_major_axes);
        y = 1/atof(diametar);
        asteroids[i].X=x;
        asteroids[i].Y=y;
        asteroids[i].errY=atof(sigma_D);

        fprintf(dest, "%f, %f\n", x, y);
        i++;
    }

    if(i<(*numOfAsteroids) || (*numOfAsteroids)==0)
    {
        printf("warnning: number of asteroids set based on num of asteroids in file\n          ");
        (*numOfAsteroids) = i;
        printf("number of asteroids: %d\n", (*numOfAsteroids));
    }

    fclose(source);
    fclose(dest);
    return 0;
}

void printToFile(const char *srcDest, struct Asteroid *asteroids, int numOfAsteroids)
{
    int i;
    FILE *dest = fopen(srcDest, "w+");

    for(i=0; i<numOfAsteroids; i++)
    {
        fprintf(dest,"%f, %f\n", asteroids[i].X, asteroids[i].Y);
    }

    fclose(dest);
}

void printFittingLine(struct FittingLineParameters *fittingLine)
{
    printf("-------------------------------\n");
    printf("best fit:  y=%f%s%f * x\n",fittingLine->c0, (fittingLine->c1>0 ? "+" : ""), fittingLine->c1);
    printf("error: %f\n", fittingLine->greska);
    //printf("broj binova: %d\n",fittingLine->num_of_bins);
    printf("-------------------------------\n");
}

int findNonWeightedCenter(struct Asteroid *asteroids, double *centar, int numOfAsteroids)
{
    int i;
    double sum = 0.0;

    for(i=0; i<numOfAsteroids; i++)
    {
        sum = sum + asteroids[i].X;
    }

    *centar = sum/numOfAsteroids;

    for(i=0; i<numOfAsteroids; i++)
    {
        if(asteroids[i].X < *centar && asteroids[i+1].X > *centar)
        {
            //printf("NonWeightedCenter = %f [Index = %d]\n", *centar, i);
            return i;
        }
    }

    return -1;
}

int findWeightedCenter(struct Asteroid *asteroids, double *centar, int numOfAsteroids)
{
    int i;
    double sum = 0.0;
    double sumD = 0.0;

    for(i=0; i<numOfAsteroids; i++)
    {
        sumD = sumD + pow(1/asteroids[i].Y, 3);
    }

    for(i=0; i<numOfAsteroids; i++)
    {
        sum = sum + asteroids[i].X/pow(asteroids[i].Y, 3);
    }

    *centar = sum/sumD;

    for(i=0; i<numOfAsteroids; i++)
    {
        if(asteroids[i].X < *centar && asteroids[i+1].X > *centar)
        {
            //printf("WeightedCenter = %f [Index = %d]\n", *centar, i);
            return i;
        }
    }

    return -1;
}

int findNextToPickedCenter(struct Asteroid *asteroids, double center, int numOfAsteroids)
{
    int i;
    // to find Jablunka, we will find asteroid with smalest value of y (this will be the biggest asteroid 1/D !!!
    //qsort by Y axis to devide in bins
    //qsort(asteroids, numOfAsteroids, sizeof(struct Asteroid), sortByY);

    //qsort by X axis
    qsort(asteroids, (size_t)numOfAsteroids, sizeof(struct Asteroid), sortByX);

    //find Jablunka's index
    for(i=0; i<numOfAsteroids; i++)
    {
        if(center > asteroids[i].X && center < asteroids[i+1].X)
        {
            printf("Centar=%f\nCentarIndex=%d\n", center, i);
            return i;
        }
    }

    printf("Error: Center not found. Chech if center you entered is within range of asteroids\nx axis: [%f, %f]\n", asteroids[0].X, asteroids[numOfAsteroids-1].X);

    qsort(asteroids, (size_t)numOfAsteroids, sizeof(struct Asteroid), sortByY);

    return -1;
}

void findMinIndex(struct Asteroid *asteroids, int size, int *min, int *minSec)
{
    int i;

    for(i=0; i<size; i++)
    {
        if(asteroids[*min].X > asteroids[i].X)
	    {
	        *minSec = *min;
            *min=i;
	    }
    }

}

void findMaxIndex(struct Asteroid *asteroids, int size, int *max, int *maxSec)
{
    int i;

    for(i=0; i<size; i++)
    {
        if(asteroids[*max].X < asteroids[i].X)
	    {
		    *maxSec = *max;
		    *max=i;
	    }
    }

}

void takeNextChunk(struct Asteroid *asteroids, int start, int size, struct Asteroid *chunk)
{
    int i;

    for(i=0; i<size; i++)
    {
        chunk[i].X = asteroids[start+i].X;
        chunk[i].Y = asteroids[start+i].Y;
        chunk[i].errY = asteroids[start+i].errY;
        //printf("chunk[%d] = %f, %f\n", i, chunk[i].X, chunk[i].Y);
    }

}

void deterimineLeftBins(struct Asteroid *asteroids, int end, int num_of_bins, struct Asteroid *selectedAsteroids,
                        double c0, double c1, double cov00, double cov01, double cov11, double *Y)
{
    int i = 0;
    struct Asteroid peakAsteroid;

    int numberOfAsteroidsInBin = end/num_of_bins;

    qsort(asteroids, (size_t)end, sizeof(struct Asteroid), sortByY);

    peakAsteroid.X = asteroids[0].X;
    peakAsteroid.Y = asteroids[0].Y;
    peakAsteroid.errY = asteroids[0].errY;

    while(i<num_of_bins)
    {
        int min=0, minSec=0;
        double minErr, minSecErr;

        struct Asteroid *chunkOfAsteroids = (struct Asteroid*)malloc(((end/num_of_bins)+1)*sizeof(struct Asteroid));

	    takeNextChunk(asteroids, i*numberOfAsteroidsInBin, numberOfAsteroidsInBin, chunkOfAsteroids);
	    findMinIndex(chunkOfAsteroids, numberOfAsteroidsInBin, &min, &minSec);

        if(c0==0 && c1==0) {
            printf("prvi prolazak\n");
            selectedAsteroids[i].X = chunkOfAsteroids[min].X;
            selectedAsteroids[i].Y = chunkOfAsteroids[min].Y;
            selectedAsteroids[i].errY = chunkOfAsteroids[min].errY;
        } else
        {
            printf("drugi prolazak\n");
            gsl_fit_linear_est(chunkOfAsteroids[min].X, c0, c1, cov00, cov01, cov11, Y, &minErr);
            gsl_fit_linear_est(chunkOfAsteroids[minSec].X, c0, c1, cov00, cov01, cov11, Y, &minSecErr);

            if(minErr>minSecErr && selectedAsteroids[i].Y < c0+c1*selectedAsteroids[i].X)
            {
                selectedAsteroids[i].X = chunkOfAsteroids[minSec].X;
                selectedAsteroids[i].Y = chunkOfAsteroids[minSec].Y;
                selectedAsteroids[i].errY = chunkOfAsteroids[minSec].errY;
            } else{
                selectedAsteroids[i].X = chunkOfAsteroids[min].X;
                selectedAsteroids[i].Y = chunkOfAsteroids[min].Y;
                selectedAsteroids[i].errY = chunkOfAsteroids[min].errY;
            }

        }

        i++;
        free(chunkOfAsteroids);
    }

    selectedAsteroids[num_of_bins].X=peakAsteroid.X;
    selectedAsteroids[num_of_bins].Y=peakAsteroid.Y;
    selectedAsteroids[num_of_bins].errY=peakAsteroid.errY;
}


void deterimineRightBins(struct Asteroid *asteroids, int start, int numOfAsteroids, int num_of_bins, struct Asteroid *selectedAsteroids,
                         double c0, double c1, double cov00, double cov01, double cov11, double *Y)
{
    int i = 0;
    struct Asteroid peakAsteroid;

    int numberOfAsteroidsInBin = abs(numOfAsteroids-start)/num_of_bins;

    qsort(asteroids, (size_t)(numOfAsteroids-start), sizeof(struct Asteroid), sortByY);

    peakAsteroid.X = asteroids[0].X;
    peakAsteroid.Y = asteroids[0].Y;
    peakAsteroid.errY = asteroids[0].errY;

    while(i<num_of_bins)
    {
	    int max=0, maxSec=0;
        double maxErr;
        double maxSecErr;

        struct Asteroid *chunkOfAsteroids = (struct Asteroid*)malloc((numberOfAsteroidsInBin+1)*sizeof(struct Asteroid));
        takeNextChunk(asteroids, i*numberOfAsteroidsInBin, numberOfAsteroidsInBin, chunkOfAsteroids);
    	findMaxIndex(chunkOfAsteroids, numberOfAsteroidsInBin, &max, &maxSec);

        if(c0==0 && c1==0) {
            selectedAsteroids[i].X = chunkOfAsteroids[max].X;
            selectedAsteroids[i].Y = chunkOfAsteroids[max].Y;
            selectedAsteroids[i].errY = chunkOfAsteroids[max].errY;
        } else
        {
            gsl_fit_linear_est(chunkOfAsteroids[max].X, c0, c1, cov00, cov01, cov11, Y, &maxErr);
            gsl_fit_linear_est(chunkOfAsteroids[maxSec].X, c0, c1, cov00, cov01, cov11, Y, &maxSecErr);

            if(maxErr>maxSecErr && selectedAsteroids[i].Y<c0+c1*selectedAsteroids[i].X)
            {
                selectedAsteroids[i].X = chunkOfAsteroids[maxSec].X;
                selectedAsteroids[i].Y = chunkOfAsteroids[maxSec].Y;
                selectedAsteroids[i].errY = chunkOfAsteroids[maxSec].errY;
            } else{
                selectedAsteroids[i].X = chunkOfAsteroids[max].X;
                selectedAsteroids[i].Y = chunkOfAsteroids[max].Y;
                selectedAsteroids[i].errY = chunkOfAsteroids[max].errY;
            }
        }

	    free(chunkOfAsteroids);
        i++;
    }

    selectedAsteroids[num_of_bins].X = peakAsteroid.X;
    selectedAsteroids[num_of_bins].Y = peakAsteroid.Y;
    selectedAsteroids[num_of_bins].errY = peakAsteroid.errY;
}

int leftFit(struct Asteroid *asteroids, int centarIndex, struct FittingLineParameters *fittingLineLeft)
{
    int i;
    double y_err;
	double c0, c1, cov00, cov01, cov11, chisq;
    struct Asteroid *selectedAsteroids = (struct Asteroid*)malloc((numOfBinsLeft+1)*sizeof(struct Asteroid));

    double *X = (double*)malloc((numOfBinsLeft+1)*sizeof(double));
    double *Y = (double*)malloc((numOfBinsLeft+1)*sizeof(double));
    double *errY = (double*)malloc((numOfBinsLeft+1)*sizeof(double));

    deterimineLeftBins(asteroids, centarIndex, numOfBinsLeft, selectedAsteroids, 0, 0, 0, 0, 0, Y);

	for(i=0;i<numOfBinsLeft+1;i++)
	{
		X[i]=selectedAsteroids[i].X;
		Y[i]=selectedAsteroids[i].Y;
        errY[i]=selectedAsteroids[i].errY;
    }

    //gsl_fit_wlinear (X, 1, errY, 1, Y, 1, (const size_t)numOfBinsLeft+1, &c0, &c1, &cov00, &cov01, &cov11, &chisq);
	gsl_fit_linear(X, 1, Y, 1, (const size_t)numOfBinsLeft+1,&c0, &c1, &cov00, &cov01, &cov11,&chisq);

    //correct line
    deterimineLeftBins(asteroids, centarIndex, numOfBinsLeft, selectedAsteroids, c0, c1, cov00, cov01, cov11, Y);

    //calculate again
    for(i=0;i<numOfBinsRight+1;i++)
    {
        X[i]=selectedAsteroids[i].X;
        Y[i]=selectedAsteroids[i].Y;
        errY[i]=selectedAsteroids[i].errY;
    }

    //gsl_fit_wlinear (X, 1, errY, 1, Y, 1, (const size_t)numOfBinsLeft+1, &c0, &c1, &cov00, &cov01, &cov11, &chisq);
    gsl_fit_linear(X, 1, Y, 1, (const size_t)numOfBinsLeft+1, &c0, &c1, &cov00, &cov01, &cov11, &chisq);

    //finish
    gsl_fit_linear_est((1-c0)/c1, c0, c1, cov00, cov01, cov11, Y, &y_err);
	fittingLineLeft->c0 = c0;
	fittingLineLeft->c1 = c1;
	fittingLineLeft->greska = y_err;

	printToFile("IzabraniLevi.txt", selectedAsteroids, numOfBinsLeft+1);
	printFittingLine(fittingLineLeft);

	free(X);
	free(Y);

    return 0;
}

int rightFit(struct Asteroid *asteroids, int centarIndex,  struct FittingLineParameters *fittingLineRight)
{
	int i;
	double c0, c1, cov00, cov01, cov11, chisq;
    struct Asteroid *selectedAsteroids = (struct Asteroid*)malloc((numOfBinsRight+1)*sizeof(struct Asteroid));

    double *X = (double*)malloc((numOfBinsRight+1)*sizeof(double));
    double *Y = (double*)malloc((numOfBinsRight+1)*sizeof(double));
    double *errY = (double*)malloc((numOfBinsLeft+1)*sizeof(double));

	deterimineRightBins(asteroids, centarIndex, numOfAsteroids, numOfBinsRight, selectedAsteroids, 0, 0, 0, 0, 0, Y);

	for(i=0;i<numOfBinsRight+1;i++)
	{
		X[i]=selectedAsteroids[i].X;
		Y[i]=selectedAsteroids[i].Y;
        errY[i]=selectedAsteroids[i].errY;
	}

    //gsl_fit_wlinear (X, 1, errY, 1, Y, 1, (const size_t)numOfBinsLeft+1, &c0, &c1, &cov00, &cov01, &cov11, &chisq);
    gsl_fit_linear(X, 1, Y, 1, (const size_t)numOfBinsRight+1, &c0, &c1, &cov00, &cov01, &cov11, &chisq);

    //correct line
    deterimineRightBins(asteroids, centarIndex, numOfAsteroids, numOfBinsRight, selectedAsteroids, c0, c1, cov00, cov01, cov11, Y);

    //calculate again
    for(i=0;i<numOfBinsRight+1;i++)
    {
        X[i]=selectedAsteroids[i].X;
        Y[i]=selectedAsteroids[i].Y;
        errY[i]=selectedAsteroids[i].errY;
    }

    //gsl_fit_wlinear (X, 1, errY, 1, Y, 1, (const size_t)numOfBinsLeft+1, &c0, &c1, &cov00, &cov01, &cov11, &chisq);
    gsl_fit_linear(X, 1, Y, 1, (const size_t)numOfBinsRight+1, &c0, &c1, &cov00, &cov01, &cov11, &chisq);

    //finish
    fittingLineRight->c0 = c0;
	fittingLineRight->c1 = c1;

	double y_err;
	gsl_fit_linear_est((1-c0)/c1, c0, c1, cov00, cov01, cov11, Y, &y_err);
	fittingLineRight->greska = y_err;
	printToFile("IzabraniDesni.txt", selectedAsteroids, numOfBinsRight+1);

    printFittingLine(fittingLineRight);

	free(X);
	free(Y);

    return 0;
}


void calculateAge(struct FittingLineParameters fittingLineFinal, double center, int *age, int *ageError)
{
     // vrednost a (tj.x) za 1/D tj. y=1/D; koristimo levu pravu
     // ako znamo da Jarkovski promeni veliku poluosu asteroida precnika 1km za 0.0005AJ, onda:
     // promena velike poluose   ----  x godina
     // 0.0005                   ----  1 milion godina

     double a_endDesno = center;
     double a_endLevo = 1/fittingLineFinal.c1 - fittingLineFinal.c0/fittingLineFinal.c1;

     *age = (int)(fabs(a_endLevo-a_endDesno)/0.00053);
     *ageError = (int)(fittingLineFinal.greska*(*age));
printf("y err %f\n", fittingLineFinal.greska);
     printf("Age of a family: %d +/- %f millions of years\n", *age, fittingLineFinal.greska);
     printf("-------------------------------\n");

}


void fittingRightAndLeft(struct Asteroid *asteroids, int numOfAsteroids, int centarIndex)
{
    char path[50];
    int ageLeft, ageErrorLeft, ageRight, ageErrorRight;
	double x = asteroids[centarIndex].X;

	struct FittingLineParameters fittingLineLeft;
	struct FittingLineParameters fittingLineRight;

	//ubaceno radi iscrtavanja u graf
	printToFile("Levi.txt", asteroids, centarIndex);
	printToFile("Desni.txt", asteroids+centarIndex, numOfAsteroids-centarIndex);

    leftFit(asteroids, centarIndex, &fittingLineLeft);
    rightFit(asteroids+centarIndex, centarIndex, &fittingLineRight);

	calculateAge(fittingLineLeft, x, &ageLeft, &ageErrorLeft);
    calculateAge(fittingLineRight, x, &ageRight, &ageErrorRight);

	strcpy(path, GNUPLOTPATH);
	strcat(path, " -persistent");

    FILE * gnuplotPipe = popen(path, "w");
    fprintf(gnuplotPipe, "set title \"Starost levo: %d +/- %d myr           Starost desno: %d +/- %d myr\"; \
			   set xrange [%f:%f]; \
			   set yrange [%f:%f]; \
			   plot \"Desni.txt\" using 1:2 title \"Asteroidi desno od centra\", \
				\"Levi.txt\" using 1:2 title \"Asteroidi levo od centra\", \
				\"IzabraniDesni.txt\" using 1:2 title \"Asteroidi koje fitujemo - desno\", \
				\"IzabraniLevi.txt\" using 1:2 title \"Asteroidi koje fitujemo - levo\", \
				%f%s%f * x, %f%s%f * x\n", ageLeft, ageErrorLeft, ageRight, ageErrorRight,
							  2.2, 2.5, 0.0, 1.0,
							  fittingLineLeft.c0, (fittingLineLeft.c1>0 ? "+" : ""), fittingLineLeft.c1,
							  fittingLineRight.c0, (fittingLineRight.c1>0 ? "+" : ""), fittingLineRight.c1);

	pclose(gnuplotPipe);
	remove("Levi.txt");
	remove("Desni.txt");
	remove("Asteroidi.txt");
	remove("IzabraniLevi.txt");
	remove("IzabraniDesni.txt");

}


int parseArgs(int argc, char* argv[], struct Task* task)
{
    if(argc!=5)
    {
        printf("\n\nError: Invalid number of arguments.\nTry using:\nfit <filename> <numberOfAsteroids> <PickCenter|NonWeightedCenter|WeightedCenter> <Right|Left|RightAndLeft|CombinedWithCenter>.\n\n");

        printf("Age calculation of asteroid family will be done based on \ndiameters of asteroids and their semimajor axises.\n");
        printf("By estimating fitting lines of asteroid family, estimation\non how mouch would asteroid with diameter of 1km\nchange its semimajor axis in billion years will be done.\n\n");
        printf("For method to be sucessful, these fitting lines should form V shape.\n");
        printf("You can choose to count age in few diferent ways.\n");
        printf("Choose how center will be calculated using one of these:\n");
        printf("  PickCenter         - choose your own center (this could be x axis\n                       of biggest asteroid of family)\n");
        printf("  NonWeightedCenter  - calculate center as avrage of all semimajor\n                       axises in family\n");
        printf("  WeightedCenter     - calculate weighted center based on semimajos\n                       axises and diameters\n\n");
        printf("Choose how fitting will be calculated using one of these:\n");
        printf("  Left               - calculate age based only on left  part of V shape\n");
        printf("  Right              - calculate age based only on right part of V shape\n");
        printf("  RightAndLeft       - calculate age for both left and right part of\n                       V shape separatly\n");
        printf("  CombinedWithCenter - calculate age using center to fit both left \n                       and right side to name tilt\n");

        return -1;
    }
    else
    {
        strcpy(task->filename, argv[1]);

        if(strcmp(argv[3], "PickCenter")==0)
            task->centering = PickCenter;
        else if(strcmp(argv[3], "NonWeightedCenter")==0)
            task->centering = NonWeightedCenter;
        else if(strcmp(argv[3], "WeightedCenter")==0)
            task->centering = WeightedCenter;
        else
        {
            printf("Error: Invalid centering argument.\nfit <filename> <numberOfAsteroids> <PickCenter|NonWeightedCenter|WeightedCenter> <Right|Left|RightAndLeft|CombinedWithCenter>.\n\n");
            return -1;
        }

    }
    return 0;
}


int main(int argc, char* argv[])
{

    struct Task task;
    int centarIndex;
    double centar=0;
    struct Asteroid *asteroids = NULL;

    printf("\nWelcome!\n");
    printf("-------------------------------\n");
    printf("This software is designed to calculate age of\nasteroid families using Jarkovski effect.\n");
    if(parseArgs(argc, argv, &task)!=0)
    {
        printf("\n\nAgruments parse failed.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }

    asteroids = (struct Asteroid*)malloc(numOfAsteroids*sizeof(struct Asteroid));
    if(parceFamilyFromFile(asteroids, argv[1], &numOfAsteroids) < 0)
    {
        printf("File parse failed.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }
    qsort(asteroids, (size_t)numOfAsteroids, sizeof(struct Asteroid), sortByX);

    //find center of family
    char centerStr[50];
    switch (task.centering)
    {
        case PickCenter:
                printf("Enter x axis of center: ");
                scanf("%s", (char*)&centerStr);
                //centar = atof(centerStr);
                centarIndex = findNextToPickedCenter(asteroids, atof(centerStr), numOfAsteroids);
                break;
        case NonWeightedCenter:
                centarIndex = findNonWeightedCenter(asteroids, &centar, numOfAsteroids);
                break;
        case WeightedCenter:
                centarIndex = findWeightedCenter(asteroids, &centar, numOfAsteroids);
                break;
        default:
                printf("\nError: Centering not determined.\n");
                printf("Ending program. Bye...\n");
                return 0;
                //break;
    }

    if(centarIndex<0 || centar==0)
    {
        printf("\nError: Center not determined.\n");
        printf("Ending program. Bye...\n");
        return 0;
    }
    printf("-------------------------------\n");
    printf("Center of a family: %f\ncentarIndex %d\n", centar, centarIndex);
    // end of finding center of family

    fittingRightAndLeft(asteroids, numOfAsteroids, centarIndex);
    printf("Success: Age calculated.\nEnding program. Bye...\n");

    return 0;

}
